# Task 2 - EKS
#### Docker image:

Docker image is already built and pushed to Dockerhub.
Image is public.
Image name is: vladam991/express-ver-1

Command used to build image from local Dockerfile(must be in project root):

**docker build -t express-ver-1 .**

When AWS cloud infrastructure is set up using Terraform, use next commands to deploy application to eks kluster:

#### Kuberentes deployment:

* aws eks update-kubeconfig --region eu-central-1 --name my_cluster
* kubectl apply -f ./kubernetes/ns.yaml -n verify-cluster
* kubectl apply -f ./kubernetes/app-config.yaml -n my-namespace
* kubectl apply -f ./kubernetes/app-secret.yaml -n my-namespace
* kubectl apply -f ./kubernetes/deployment.yaml -n my-namespace
* kubectl apply -f ./kubernetes/service.yaml -n my-namespace
* kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.0/deploy/static/provider/cloud/deploy.yaml
* kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
* kubectl apply -f ./kubernetes/horizontal-pod-autoscaler.yaml -n my-namespace
* kubectl apply -f ./kubernetes/app-ingress.yaml -n my-namespace

After new image is pushed, to apply changes, use command:
kubectl rollout restart deployment express-app-deployment -n my-namespace

#### Test:
Public dns of load balancer is shown on load balancer section on AWS.
Copy public dns and go to browser and paste it in url section.

In order to check if env variables are read correctly in pods, use next url:

http://public-dns-load-balancer/list-env-variables

In order to test horizontal pod autoscaler, tools such as k6 could be used targeting either "/" or "/list-env-variables" routes.





 








