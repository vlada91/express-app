const express = require('express');

const app = express();

app.get('/', (req,res) => {
    res.send("Zdravo svete!");
});

app.get('/list-env-variables', (req, res) => {
    const myConfigMapVaribale = process.env.configMapVariable;
    const mySecretMapVariable = process.env.secretMapVariable;
    res.send(`myConfigMapVaribale is: ${myConfigMapVaribale}, mySecretMapVariable is ${mySecretMapVariable}`);
});

app.listen(8080);


