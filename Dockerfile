FROM node:12

RUN groupadd -r myuser && useradd -r -g myuser myuser

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY app.js ./

RUN chown -R myuser:myuser /usr/src/app

USER myuser

EXPOSE 8080

CMD [ "node", "app.js" ]
